**<- [WSL](/pt/WSL)** - **[Resumo](/pt/home)** - **[Descrição](pt/Descrição) ->**

# DOS processing

This older version runs under DOS in a "command.com" window and allows to perform batch processing. 
Batch processing is currently done by the Perl script process.pl but it could easily be replaced by a Python script, or an executable, simplifying the configuration here.

The processing of a CTD/LADCP station is done in three steps:

- copy.bat script that performs the copy of raw files from Seasave (CTD) and bbtalk (LADCP) to CRDAP data-raw and data-processing directories.
- conversion and processing of CTD files with the Perl script `process.pl` using the SBEDataProcessing programs
- manual launch of LADCP profile processing with Matlab script `process_cast(xxx)`. 

## Programs installation and tools

Install the Perl language from the ActiveState distribution :
[https://www.activestate.com/products/perl/downloads/](https://www.activestate.com/products/perl/downloads/)

From this page, you will have to create an account and configure the installer (OS, additional package, etc)
Or more simply, install the "state" tool, with the following powershell command:

Run the following command as an Administrator in your Command Prompt to install it:

    powershell "IEX(New-Object Net.WebClient).downloadString('https://platform.activestate.com/dl/cli/install.ps1')"

Set this project up locally with the following command:

    state activate <your login>/CTD
	
Install also the bzip2 and gawk utilities for Windows, from the GnuWin32 site, installation binaries should be download under :

- [http://gnuwin32.sourceforge.net/packages/bzip2.html](http://gnuwin32.sourceforge.net/packages/bzip2.htm)
- [http://gnuwin32.sourceforge.net/packages/gawk.htm](http://gnuwin32.sourceforge.net/packages/gawk.htm)

The `process.pl` processing script calls at the end of the script SbeConReport.exe program, so we need to add the path of SBEDataProcessing-Win32 in the Windows path, as well as the path of the GnuWin utilities :

    Path=C:\Program Files (x86)\Sea-Bird\SBEDataProcessing-Win32;C:\Program Files (x86)\GnuWin32\bin;
	
You can use the Rapid Environment Editor tool (Windows environment variables management):

- [https://www.rapidee.com/en/download](https://www.rapidee.com/en/download)

# Configuration

## Setting the environment variables

For a quick access to the processing directory and to the script, we have to define the following v variables as you would do in Linux on the command line or in your `autoexec.bat` file so that they are defined once and for all.

    set CRUISE=PIRATA-FR32
    set CRUISE_FORMAT=fr32%03d
    set PREFIXM=FR32
    set DRIVE=c:\Cruises\PIRATA
    set CTD=%DRIVE%\%CRUISE%\data-processing\CTD

and update the path:

    setx path "%path%;%DRIVE%\%CRUISE%\local\sbin\dos"

or if the path exeed 1024 caracters, use 

The `process.pl` script uses exactly the same configuration and directories as the Matlab ctdSeaProcessing GUI. 

# Usage

Open a `cmd` window, define the environment variables for the cruise, move to the `data-processing\CTD` directory then launch the processing of station 001:

```DOS
set CRUISE=PIRATA-FR32
set PREFIX=fr32
set PREFIXM=FR32
set CRUISE_FORMAT=%PREFIX%%03d
set DRIVE=c:\Cruises\PIRATA
set CTD=%DRIVE%\%CRUISE%\data-processing\CTD
cd %CTD%
synchro-ctd.bat 001
```

![image](uploads/c38d4156719eb371587de3440a5b1266/image.png)


```DOS
process.pl 001
```

![image](uploads/5fefb2c0c5af77d1e5b6a81153d22eb0/image.png)

The program `process.pl` can take several options in parameters, in order to do only a part of the processing, or to process several stations. 

It is also convenient to use the option "#w" when it is necessary to modify the configuration of the CTD sensors.

```DOS
c:\Cruises\PIRATA\PIRATA-FR32\data-processing\CTD>process.pl -h

usage:   perl process.pl [options] first_station [last_station|"#w"]
Options:
    --help                 Display this help message
    --debug                display command, don't process
    --btl                  process sample
    --codac                process realtime data for CODAC
    --pmel                 process realtime data for PMEL
    --ladcp                process LADCP data
    --std|ctd              process Seabird cast
    --plot                 process plots
    --report               process Seabird report
    --all                  process all steps, default

example: perl process.pl 1

example: perl m:\ABRACOS2\data-processing\CTD\process.pl 01

example: perl m:\ABRACOS2\data-processing\CTD\process.pl 01 "#w"

example: perl m:\ABRACOS2\data-processing\CTD\process.pl 01 09 --std
```
**<- [WSL](/pt/WSL)** - **[Resumo](/pt/home)** - **[Descrição](pt/Descrição) ->**