# Overview

Windows Subsystem for Linux (WSL) lets developers run a GNU/Linux environment, including most command-line tools, utilities, and applications, directly on Windows, unmodified, without the overhead of a traditional virtual machine or dual-boot setup.

# Install WSL

You must be running Windows 10 version 2004 and higher (Build 19041 and higher) or Windows 11.

Before installing Linux distributions for WSL, you must ensure that the optional "Windows Subsystem for Linux" feature is enabled on your PC:

Open PowerShell as an administrator and run: 

    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
	
Restart your computer when prompted.

Install the new Windows terminal from the Microsoft Store which will replace the command prompt. The current version is called "Windows Terminal".

![image](uploads/77c7d0d611e6d4d05ada400623a2c552/image.png)

See the documentation, "install and tutorials" on the Microsoft website.

[https://docs.microsoft.com/en-us/windows/wsl/install](https://docs.microsoft.com/en-us/windows/wsl/install)

You can also install a [Ubuntu 20.04](https://www.microsoft.com/en-us/p/ubuntu-2004/9n6svws3rx71#activetab=pivot:overviewtab) distribution directly from the Microsoft store.

![image](uploads/8870c6217187ff97f74cc79399c1d3b0/image.png)

See [https://ubuntu.com/wsl](https://ubuntu.com/wsl)

The first step you are asked to perform when opening the newly installed Linux distribution is to create an account with a username and password. This username and password are specific to your Linux distribution and do not affect your Windows username. Once you have created this username and password, the account becomes your default user for the distribution and automatically logs in at boot time.

This account is considered the Linux administrator, with the ability to run sudo (Super User Do) administrative commands.

Once the Linux (Ubuntu) session is started and the user account is created, you can launch the Windows Terminal. This one opens by default a DOS session. 

![image](uploads/1338e4df49200a1c22cfc1a39433804d/image.png)

In the menu (on the right of the tab, down arrow), select the installed distribution, here: Ubuntu-20.04 :

![image](uploads/b001566c51fc28066af63503aaba977b/image.png)

# Configuration

Update the distribution: 

    sudo apt update
    sudo apt upgrade
	
Install a light X environment such as Xfce (optional) if you want to do a graphical display. Install only the Xfce environment with the command:

    sudo apt install xfce4 
    sudo apt install vim-gtk

**WSL1** Edit the ~/.bashrc file and add the DISPLAY environment variable to redirect the graphic display to the X Windows server:

    export DISPLAY=:0.0

**WSL2** has its own IP address and does not yet share the localhost lookup address (127.0.0.1); when you connect to 127.0.0.1 in WSL2, you are actually connecting to a WSL2 virtual machine rather than the underlying Windows machine. You must therefore use the IP address assigned to Windows directly.
We recommend that you use the internal IP address that is dynamically extracted and assign it to the DISPLAY environment variable, a code to be added to your .bashrc initialization file:

    export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0: 

Reload the ~/.bashrc file to take into account the new DISPLAY variable:
    
	. ~/.bashrc

Install the free X server VcXsrv under Windows to display graphical output under X. See details at: [https://doc.ubuntu-fr.org/wsl](https://doc.ubuntu-fr.org/wsl)

You have to run the XLaunch program to configure the X server before the first use, you have to disable the access control (check the box: "Disable server access control")

![image](uploads/113a30270a8c5427a539d31b6ad3b542/image.png)

Otherwise, you may get the following errors: 

    $ xeyes
    Authorization required, but no authorization protocol was specified
    Error: Can't open display: localhost:0.0

Start the X server under Windows then in a bash window under Linux, launch the xeyes program to test the display, the pupils must follow your mouse:

![image](uploads/7c7bb3df9a87c8cef32f75ca36af3c90/image.png)

Then install the following packages that will be needed for further processing of the scientific data used in the CRDAP repository: 

    sudo apt install -y gcc g++ make dos2unix netcdf-bin libnetcdf-dev perl-doc libswitch-perl libdate-manip-perl libxml-libxml-perl libconfig-tiny-perl pdl libpdl-netcdf-perl python3-pip

Install the Perl Oceano module if you use the scripts of the CRDAP repository:

```bash
mkdir -p ~/local/src
cd ~/local/src
git clone https://forge.ird.fr/us191/oceano.git
cd oceano/lib/perl/Oceano
perl Makefile.PL
make
sudo make install
```

# Installation and configuration of Python with Miniconda

Miniconda is a minimal installer for Conda that allows you to manage libraries for developing in Python, especially for data analysis. It is a reduced version of Anaconda that includes only conda, a version of Python 3.x more recent than the Ubuntu 20.04 distribution and the packages they depend on.

Compared to the pip package manager, conda is very efficient. It manages the version of Python and the packages compatible with it in an optimal way.

## Installation and configuration of miniconda

For the installation of Miniconda, we assume that you will install it as a user, not as an administrator. In other words, you will not need special rights to install Miniconda and other necessary modules. :

```bash
mkdir ~/miniconda
cd ~/miniconda
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
```

In a shell, run the Miniconda installation with the command and answer the following questions:

    bash Miniconda3-latest-Linux-x86_64.sh -U

Restart your shell so that it takes into account the initialization procedure of conda.

    . ~/.bashrc

Test the version of python and conda :

```python
python
Python 3.7.6 (default, Jan  8 2020, 19:59:22)
[GCC 7.3.0] :: Anaconda, Inc. on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> quit()

conda -V
conda 4.8.3
```
## Installation of Python scientific modules with Miniconda in (base) environment:

```bash
conda update conda
conda install pandas
conda install -c conda-forge iris xarray toml netCDF4 simplekml
pip install julian lat-lon-parser
```


## Disk mounting/sharing

The internal disks of Windows are automatically mounted under /mnt/, that is /mnt/c and /mnt/d on my laptop which has 2 SSD.
To mount a network share, an external disk or a USB key, you just have to enter the following command to mount the 2TB E: disk under /mnt/e :

    sudo mount -t drvfs e: /mnt/e

Mount the root of the Linux distribution in Windows Explorer as an X drive:

    net use X: \\wsl$\Ubuntu-18.04 /PERSISTENT:YES

or directly accès to Linux distrib with \\wsl$ share:

![image](uploads/f663666a30bce2f6ca76571796c64222/image.png)

The root directory:

![image](uploads/173a5d218698c4034a7b48ccf1e94b4a/image.png)



For more information on wsl, see the [blog](https://us191.ird.fr/spip.php?article77)
