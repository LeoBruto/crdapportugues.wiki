# CTD/LADCP processing

The CTD + LADCP data are recorded locally on the acquisition PC and then copied to the network drive by the Matlab program `ctdSeaProcessing` after each profile. 

The Matlab script `ctdSeaProcessing` also allows the processing of CTD (via SBEDataProcessing) and LADCP (via Visbeck software) profiles. The software uses the configuration file `ctdSeaProcessing_CRUISEid.ini` under data-processing which must be modified at the beginning of the cruise. The software is then launched at the end of each CTD cast with Matlab.

**Important**: All processing is done from data copied on the network, not locally. This way you can be sure that you will have all the data you need to perform the processing again after duplicating and saving the data. 

# Starting Matlab processing

To carry out CTD/LADCP data processing, go to the data-processing directory Add the directory `data-processing\ctdSeaProcessing` in the Matlab path and run the script `ctdSeaProcessing.m`.

<div align="center">

![image](uploads/325f32fd35eb99b19c97ca4467f04afa/image.png)

</div>

- All the necessary parameters are defined in an .ini configuration file, which must be read the first time it is used. The configuration is then saved in the file ctdSeaProcessing.mat. If you modify the .ini file, you will have to reload it.
- When the configuration of the CTD changes from one cruise to another, it will be necessary to launch the program **in step by step** mode.
- It will then be necessary to save the new configuration for each program. For more information, please refer to the [SBE data processing User manual](https://www.seabird.com/asset-get.download.jsa?code=251446) from Seabird.

<div align="center">

![image](uploads/cb7e7e66a678d103a30007c5c5692c47/image.png)

![image](uploads/0950cc5e308e30fa2f6aa18eafd2e57f/image.png)

![image](uploads/bff3f0aac44f1a785de2d03e96fcf3b4/image.png)

![image](uploads/5afd2f0baffc78c6385382c6918ce4e1/image.png)

</div>

# Data extraction and output

Currently, there is a generic Perl script for each instrument. The scripts are in the folders for each instrument under `data-processing`. They allow the formatting of data in different formats: ASCII, XML, ODV. The configuration of the scripts is described in an external file `config.ini` located at the root of the `data-processing` directory. This `config.ini` file must be modified at the beginning of the cruise in order to correctly fill in the global attributes and according to the configuration of the measurement instruments. Each section ([ctd], [ctd-all], [btl],...) describes the structure of the data to be extracted in the files. This formatting is done with the [`process-all.sh`](https://forge.ird.fr/us191/crdap/-/blob/master/local/sbin/process-all.sh) script.

# Formatting the data in NetCDF format

The data are then formatted in NetCDF format via the perl script `oceano2oceansites.pl` under `$CRUISE/local/sbin/`. The created variables follow the roscop code convention. Variables can be added or updated here if necessary at the end of the script `oceano2oceansites.pl`. 

# Plot of data

Python plots are then generated. The scripts are under `$CRUISE/local/sbin/python`. They generate on the one hand the plots of the CTD, XBT and LADCP profiles, launched by the shell script `python-plots.sh`, then `plots.py`, and on the other hand the TSG track scatter plot made by `scatter.py`. These scripts can be adjusted during the cruise. These plots are performed using the `process-all.sh` script.

<div align="center">

![image](uploads/56907cb7927f5234696791c49a99c8dc/image.png)

![image](uploads/c7157f325b7f1cdde1c35791bf155ec3/image.png)

![image](uploads/616d7c09500b417c7e852506b751eacf/image.png)

![image](uploads/b931cf0a01eb34a6036d35868999307d/image.png)

</div>

Source code is available in [Github](https://github.com/jgrelet/Python-plots)

# Generation of the Google Earth KML file

Finally, a KML file is generated, included ship track, XBT and CTD pushpin with interactive plots with data profiles. This file is made with the script `oceano2klm.py`, under `$CRUISE/local/sbin/python. This file is generated with the configuration file `local.toml` under data-processing and uses the python figures previously generated.

Source code is available in [Github](https://github.com/jgrelet/oceano2kml)

## Viewing the KML file locally

To view the KML file on board, it is necessary to install a Nginx web server on the Linux PC and hve an internet connection.

### Copy the files on Nginx server locally:

First of all it is necessary to create symbolic links under the root on NGINX server. It is necessary to create a directory RESILIENCE on the NGINX web server then create the following 3 links with the command `ln -s <source< <dest>`

Either:

```bash
    $ cd /var/www/html/RESILIENCE
    $ ln -s /mnt/campagnes/RESILIENCE/data-processing/CELERITE/plots/ CELERITE
    $ ln -s /mnt/campagnes/RESILIENCE/data-processing/CTD/plots/python CTD
    $ ln -s /mnt/campagnes/RESILIENCE/data-processing/THERMO/plots/ THERMO
```

Start, stop, restart the web server:

    $ sudo service nginx <start|stop|restart>

### Configuration file config.toml

The Python script `oceano2kml.py` uses the default configuration file `config.toml` under data-processing/CTD/Tracks. This file must be modified at the beginning of the campaign according to the parameters of the campaign and the files used. The Web server does not allow to "browser" the content of a directory, it is thus necessary to enter the complete URL to visualize an image from its browser.

### Visualization of the KML file on the Web

Back in the laboratory, you need to precess `oceano2kml.py` again with a different configuration file, the kml file all the files are copied to the read-only directory of the web server 'https://www.brest.ird.fr' and the kml file can be loaded online with Google Earth from the following URL: 

[https://www.brest.ird.fr/us191/cruises/pirata-fr32/pirata-fr32.kml](https://www.brest.ird.fr/us191/cruises/pirata-fr32/pirata-fr32.kml)

![image](uploads/b890aad0e2b3965c3c8b20e533111e8c/image.png)

