**[Installation](/fr/Installation)**

# General outline of the information system

You can find detailed information (in french) on the architecture and use of CRDAP implemented during the last [PIRATA-FR31 cruise](https://drive.ird.fr/s/oegQb48aZ8p4R37) in March/April 2021:

<div align="center">

|Setting up the user environment|
| :--------------- |
|.bashrc|
|local/etc/skel/.bashrc.CRUISENAME|
|contrab|

</div>

|Data synchronization|Data formatting (ASCII,XML,ODV) |Data formatting (NetCDF)|Data visualizations|Google kml files
|:--------------- |:---------------:|:---------------:|:---------------:| ---------:|
|local/sbin/synchro.sh (onboard data)|local/sbin/process-all.sh|local/sbin/process-all.sh|local/sbin/process-all.sh|local/sbin/process-all.sh|local/sbin/process-all.sh|
|<font color='red'>data-processing/ctdSeaProcessing.ini</font> (CTD/LADCP)|data-processing/config.ini|local/sbin/oceano2oceansites.pl|local/sbin/python/python-plots.sh|oceano2python.py|
|data-processing/ctdSeaProcessing |data-processing/[CTD,XBT,...]/[ctd,xbt,…].pl||local/sbin/python/scatter.py|data-processing/local.toml|

# Main directory tree structure

```bash
├── data-final = Products ready for delivery to oceanographic data centers
├── data-adjusted = Adjusted data  
├── data-processing = Pre-processed data
├── data-raw = Raw data
├── DOCUMENTS = Cruise documents
├── local = Processing programs, scripts and synchronisation utilities
└── tmp = Temporary files
```

# Configuration of a processing session under Linux

The goal is to get in its shell environment all the aliases necessary for processing and formatting the data.

In the user's `.bashrc` file ("~/.bashrc"), at the end of the script, define the environment variables for the cruise and the file to be sourced: :

```bash
# CRUISE variables
export DRIVE=/mnt/campagnes
export CRUISE=RESILIENCE
export CRUISEid=RES

# Source CRUISE definitions
if( -f ${DRIVE}/local/etc/skel/.bashrc.${CRUISE} ) then
  source ${DRIVE}/local/etc/skel/.bashrc.${CRUISE}
endif
```

The file to be sourced is located under `${CRUISE}/local/etc/skel/.bashrc.${CRUISENAME}`. 

This file contains all the aliases necessary for the processing. This script can be modified at the beginning of the cruise to add or remove aliases.

Two shell scripts, under `${CRUISE}/local/sbin/` are used to launch all the processing tasks:

* `synchro.sh` script retrieves data from on board network and copies them to the data-raw and data-processing directories. This script must be modified at the beginning of the cruise in order to adjust to the network architecture of the ship. 

* 'process-all.sh` script will perform the extraction and archiving in different formats (ASCII, XML, ODV, NetCDF) once the collection is done by the `synchro.sh` script.


# Automation of the processing

The synchronization of the data as well as processing are done automatically with the help of the crontab. To modify its content, use the `crontab -e` command and `crontab -l` to view it. 	

```bash
$ crontab -l 

# .---------------- minute (0 - 59)
# |   .------------- hour (0 - 23)
# |   |   .---------- day of month (1 - 31)
# |   |   |   .------- month (1 - 12) OR jan,feb,mar,apr ...
# |   |   |   |  .----- day of week (0 - 7) (Sunday=0 or 7)  OR sun,mon,tue,wed,thu,fri,sat
# |   |   |   |  |
# *   *   *   *  *  command to be executed
# all processing cruise 3 times per day (local time: 8, 16, 22h)
SHELL=/bin/sh
CRUISE=RESILIENCE
DRIVE=/mnt/campagnes
CRUISEid=RES
5 9,11,17,22 * * *  /mnt/campagnes/${CRUISE}/local/sbin/synchro.sh $CRUISE $DRIVE $CRUISEid > /mnt/campagnes/${CRUISE}/local/logs/synchro.log 2>&1
30 9,11,17,22 * * * /mnt/campagnes/${CRUISE}/local/sbin/process-all.sh $CRUISE $DRIVE > /mnt/campagnes/${CRUISE}/local/logs/process.log 2>&1
0 8,12,21 * * * rsync -av --exclude '.git' /mnt/campagnes/${CRUISE}/ /media/science/WD\ 2T/campagnes/${CRUISE}/ > /mnt/campagnes/${CRUISE}/local/logs/backup.log 2>&1
```

During the cruise, check regularly the log files to see possible problems (user access and permissions, missing or new data,...)

**[Processing](Processing)**
