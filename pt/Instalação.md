**<- [Introdução](/pt/Home)** - **[Resumo](/pt/home)** - **[WSL](/pt/WSL) ->**

# CRDAP - Instalação - Versão Portuguese

## Pré-requisitos

A instalação do CRDAP requer que os seguintes softwares sejam instalados no PC Windows:

- git
- Tortoise Git
- Matlab (automação GUI processamento CTD e LADCP)
- WSL ou uma PC Linux (coleta e processamento de dados, conversão de arquivos NetCDF, traçado automático de perfis e seções)
- Baixe e instale o pacote [Seasoft](https://www.seabird.com/static/list-items/seasoft-2-3-0-en.jsp) incluindo o programa Seasave para aquisição de dados dos instrumentos Seabird (CTD) e o software SBEDataProcessing para pós-processamento de dados.
- WinADCP e RDI-Tools BBtalk para configurar, verificar e baixar dados dos LADCP dopler.

**Notas:**

- O software de aquisição de dados Seasave só pode ser executado em um PC Windows, pois utiliza a E/S do PC (serial/USB, GPIB). Entretanto, o pacote de processamento de sbe pode ser emulado em um PC Linux instalando o software com [Wine](https://wiki.winehq.org/Download) ou [Parallels](https://www.parallels.com/fr/) como Mac OsX.
- Os dados do LADCP (Lowered Acosutic Doppler Current Profiler) são processados com a versão 10.16 do software IFM-GEOMAR/LDEO (Visbeck, 2001), desenvolvido por Martin Visbeck e mantido por Gerd Krahmann.

Link para a documentação: [Aquisição e processamento de dados LADCP](https://www.ldeo.columbia.edu/~ant/LADCP.html)

Este software requer o uso de uma licença Matlab.
Também usamos o Matlab para automatizar o processamento de uma estação CTD/LADCP Seabird com uma interface gráfica de usuário (GUI).

Se você não tiver um LADCP em sua cadeia de processamento, nem uma licença Matlab, é possível realizar o processamento de Seabird sob DOS instalando Perl e algumas ferramentas Unix portadas para Windows. Veja a página dedicada a esta instalação.

## Recuperação de arquivos 

### Clonar o repositório a partir da linha de comando::

   &gt; cd \<your cruise directory\>

   &gt; git clone --recurse-submodules https://git.outils-is.ird.fr/US191/CRDAP.git .

### Com Tortoise Git:

<div align="center">

![image](uploads/e60c0002a35c49340b26908a1e834945/image.png)

</div>

O repositório será clonado, por defeito, no directório crdap (.) Pode substituir o crdap por um nome de crueziro.

<div align="center">

![image](uploads/dcb514c76913c15f1cfd638febfc49ae/image.png)

![image](uploads/7e5f401cdd4008a521dbe081e8bd1950/image.png)

![image](uploads/4342cfb2b5040e123ec5d9d5a4686b97/image.png)

</div>

Quando a cópia estiver terminada, sem erros, passar para o directório de destino.

<div align="center">

![image](uploads/d0d8612472802a3a096f4a995301a19e/image.png)

</div>

O repositório CRDAP possui dois submódulos:

* ctdSeaprocessing: uma interface Matlab para automatizar o processamento de um bathysonde CTDO2/LADCP. 
* ladcp: Módulo de processamento de dados atual LADCP.

```git
[submodule "data-processing/ctdSeaProcessing"]
	path = data-processing/ctdSeaProcessing
	url = https://github.com/US191/ctdSeaProcessing.git
    branch = master
[submodule "data-processing/LADCP/process"]
	path = data-processing/LADCP/process
	url = https://git.outils-is.ird.fr/rousselot/ladcp.git
	branch = master
```

Os dois submódulos foram baixados. Após uma atualização, será necessário atualizar os submódulos com um menu contextual, selecione "Submodule Update" (Atualização do submódulo)

<div align="center">

![image](uploads/f3289c212176374b92ab1ba0b4382738/image.png)

</div>

e clique "Ok"

<div align="center">

![image](uploads/8b556e02088182a67dbb77ed0d473e41/image.png)

![image](uploads/1bc9f412f977400904cc4dbfa7469e88/image.png)

</div>

Mudar para a filial THALASSA

com tortoise Git:

<div align="center">

![image](uploads/166d0fa7888aef3ba45d5cb8ba3e6c6b/image.png)

</div>

linha de comando:

    > checkout de git THALASSA

Mudar para a filial SWINGS

    > caixa de saída SWINGS

voltar ao filial principal master

    > git checkout master

**<- [Introdução](/pt/Home)** - **[Resumo](/pt/home)** - **[WSL](/pt/WSL) ->**
