
# Introdução

Este repositório permite a criação de todos os diretórios e ferramentas para processar os dados adquiridos no mar durante uma cruzeiro oceanográfico.

Dentro deste repositório, existem diferentes "BRANCH" correspondentes aos sistemas de aquisição presentes em cada um dos navios oceanográficos.

# Preâmbulo

Durante um cruzeiro oceanográfico, utilizamos nossos próprios instrumentos, bem como os disponíveis a bordo. Os sistemas de aquisição de dados e seus formatos são geralmente heterogêneos. A fim de simplificar o processamento e validação de dados, implementamos nos últimos anos um sistema de informação que nos permite estruturar e priorizar o acesso à informação. Este sistema pode ser adaptado a qualquer navio e duplicado no laboratório para finalizar o processamento e ajuste dos dados.

# [Instalação](pt/Instalação)

### [WSL](pt/WSL)

### [DOS](pt/DOS)

# [Descrição](pt/Descrição)

# [Processamento](pt/Processing)

# [Documentaçãoes](pt/Documentação)

# [Notas de lançamento](https://forge.ird.fr/us191/CRDAP/-/blob/master/ReleaseNotes.md)

