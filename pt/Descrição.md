**<- [Instalação](/pt/Instalação)** - **[Resumo](/pt/home)** - **[Processamento](/pt/Processamento) ->**


# Diagrama geral do sistema de informação

|Configurando o ambiente do usuário|
| :--------------- |
|.bashrc|
|local/etc/skel/.bashrc.CRUISENAME|
|contrab|


|Sincronização de dados| Formatação de dados (ASCII,XML,ODV) | Formatação de dados (NetCDF)| Visualizações de dados|Arquivos google kml|
|:--------------- |:---------------:|:---------------:|:---------------:| ---------:|
|local/sbin/synchro.sh (dados a bordo)|local/sbin/process-all.sh|local/sbin/process-all.sh|local/sbin/process-all.sh|local/sbin/process-all.sh|local/sbin/process-all.sh|
|<font color='red'>data-processing/ctdSeaProcessing.ini</font> (CTD/LADCP)|data-processing/config.ini|local/sbin/oceano2oceansites.pl|local/sbin/python/python-plots.sh|oceano2python.py|
|data-processing/ctdSeaProcessing |data-processing/[CTD,XBT,...]/[ctd,xbt,…].pl||local/sbin/python/scatter.py|data-processing/local.toml|

# Estrutura geral da árvore

```bash

├── data-final		    = Produtos acabados
├── data-adjusted		= Dados ajustados 
├── data-processing		= Dados pré-processados, o bordo
├── data-raw			= Dados brutos
├── DOCUMENTS			= Documentos de cruzeiro
├── local			    = Utilitários de processamento e sincronização
└── tmp			        = Arquivos temporários
```

# Configuração de uma sessão de processamento sob Linux

O princípio desta operação é obter em seu ambiente shell todos os aliases necessários para o processamento e formatação dos dados.

No arquivo `.bashrc` do usuário ("~/.bashrc"), no final do script, definir as variáveis de ambiente para a cruzeiro e o arquivo para teremos que execute o seguinte comando para tornar as mudanças efetivas:

```bash
# CRUISE variables
export DRIVE=/mnt/campagnes
export CRUISE=RESILIENCE
export CRUISEid=RES

# Source CRUISE definitions
if( -f ${DRIVE}/local/etc/skel/.bashrc.${CRUISE} ) then
  source ${DRIVE}/local/etc/skel/. bashrc.${CRUISE}
endif
```

O arquivo de definição está localizado em `CRUISE/local/etc/skel/.bashrc.CRUISENAME`. Este arquivo contém todos os pseudônimos necessários para o processamento. Este arquivo (roteiro) pode ser modificado no início da campanha para adicionar ou remover pseudônimos.

Dois shell scripts sob `$CRUISE/local/sbin/` são usados para lançar todas as tarefas de processamento:

* O script `synchro.sh` recupera os dados da rede a bordo e os copia para os diretórios de coleta e processamento de dados. Este roteiro deve ser modificado no início da cruzeiro para se adequar à arquitetura de rede do navio. 

* O script 'process-all.sh' executará a extração e arquivamento em diferentes formatos (ASCII, XML, ODV, NetCDF) uma vez que a coleta seja feita pelo script 'synchro.sh'.

# Automação do processamento

A sincronização e o processamento de dados são realizados automaticamente usando a crontab. Para modificar seu conteúdo, use o comando `crontab -e` e `crontab -l` para visualizá-lo.  	

```bash
$ crontab -l 

# .---------------- minute (0 - 59)
# |   .------------- hour (0 - 23)
# |   |   .---------- day of month (1 - 31)
# |   |   |   .------- month (1 - 12) OR jan,feb,mar,apr ...
# |   |   |   |  .----- day of week (0 - 7) (Sunday=0 or 7)  OR sun,mon,tue,wed,thu,fri,sat
# |   |   |   |  |
# *   *   *   *  *  command to be executed
# all processing cruise 3 times per day (local time: 8, 16, 22h)
SHELL=/bin/sh
CRUISE=RESILIENCE
DRIVE=/mnt/campagnes
CRUISEid=RES
5 9,11,17,22 * * *  /mnt/campagnes/${CRUISE}/local/sbin/synchro.sh $CRUISE $DRIVE $CRUISEid > /mnt/campagnes/${CRUISE}/local/logs/synchro.log 2>&1
30 9,11,17,22 * * * /mnt/campagnes/${CRUISE}/local/sbin/process-all.sh $CRUISE $DRIVE > /mnt/campagnes/${CRUISE}/local/logs/process.log 2>&1
0 8,12,21 * * * rsync -av --exclude '.git' /mnt/campagnes/${CRUISE}/ /media/science/WD\ 2T/campagnes/${CRUISE}/ > /mnt/campagnes/${CRUISE}/local/logs/backup.log 2>&1
```

Durante a missão, verificar regularmente os arquivos de registro para possíveis problemas (direitos de leitura, novas variáveis,...)


**<- [Instalação](/pt/Instalação)** - **[Resumo](/pt/home)** - **[Processamento](/pt/Processamento) ->**