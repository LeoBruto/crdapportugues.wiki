**<- [WSL](/pt/WSL)** - **[Resumo](/pt/home)** - **[Descrição](pt/Descrição) ->**

# Processamento DOS

Esta versão, que é mais antiga, é executada no DOS. 
Através do terminal de comando `command.com` é possível realizar o processamento de várias estações com um único comando.
O processamento "em lote" é feito atualmente pelo script Perl `process.pl`, mas poderia ser facilmente substituído por um script Python, 
ou mesmo um executável, simplificando sua utilização e configuração.

O processamento de uma estação CTD/LADCP é feito em três etapas:

- copy.bat script que copia os arquivos brutos do Seasave (CTD) e bbtalk (LADCP) para os diretórios de coleta e processamento de dados do CRDAP.
- CTD: conversão e processamento de arquivos CTD com o script Perl `process.pl xxx` utilizando o programa SBE_DataProcessing
- LADCP: para o processamento de um perfil de LADCP, usa-se o script `process_cast(xxx)` que necessita do Matlab.

## Instalação de programas e ferramentas

-PERL 
Instale a linguagem Perl a partir da distribuição ActiveState:
https://www.activestate.com/products/perl/downloads/

Para baixar o Perl, você terá que criar uma conta e configurar o instalador (sistema operacional, pacote adicional, etc.)
Outro modo de instalar o Perl é através do terminal de comando. Siga os passos abaixo:

Execute o seguinte comando como Administrador em seu Prompt de Comando para instalá-lo...

    powershell "IEX(New-Object Net.WebClient).downloadString('https://platform.activestate.com/dl/cli/install.ps1')"

Configure este projeto localmente com o seguinte comando:

    state activate <your login>/CTD
	
- Bzip2 e Gawk para Windows

Instale os utilitários bzip2 e gawk, para Windows, a partir do site GnuWin, binários de instalação a serem recuperados sob:

http://gnuwin32.sourceforge.net/packages/bzip2.htm
http://gnuwin32.sourceforge.net/packages/gawk.htm

## Adicionando os programas ao `PATH`
O script de processamento `process.pl` é chamado no final do programa `SbeConReport.exe`, para que isto funcione, o caminho da SBEDataProcessing-Win32 deve ser adicionado ao caminho do Windows, assim como o caminho dos utilitários GnuWin32:

    Path=C:\Program Files (x86)\Sea-Bird\SBEDataProcessing-Win32;C:\Program Files (x86)\GnuWin32\bin;
	
Outra forma de adicionar é pelo [Rapid Environment Editor](https://www.rapidee.com/en/download) (Windows environment variables management):

# Configuração

## Ajustando as variáveis ambientais

Para acesso rápido ao diretório de processamento e ao script, é aconselhável definir as seguintes variáveis como você faria no Linux na linha de comando ou em seu arquivo `autoexec.bat` para que sejam definidas de uma vez por todas.

    set CRUISE=PIRATA-FR32
    set CRUISE_FORMAT=fr32%03d
    set DRIVE=c:\Cruises\PIRATA
    set CTD=%DRIVE%\%CRUISE%\data-processing\CTD

e adicionar o caminho na variável `%path%` aos scripts perl e batch que estão no diretório `local\sbin\dos` 

    setx path "%path%;%DRIVE%\%CRUISE%\local\sbin\dos"

O script `process.pl` utiliza exatamente a mesma configuração e diretórios que o GUI ctdSeaProcessing do Matlab. 

# Utilisando

Abrir uma janela do prompt de comando `Win+R e digita cmd`, definir as variáveis de ambiente para a cruzeiro, entrar no diretório *processamento de dados* e então iniciar o processamento da estação 001:

```DOS
set CRUISE=PIRATA-FR32
set PREFIX=fr32
set PREFIXM=FR32
set CRUISE_FORMAT=%PREFIX%%03d
set DRIVE=c:\Cruises\PIRATA
set CTD=%DRIVE%\%CRUISE%\data-processing\CTD
cd %CTD%
synchro-ctd.bat 001
```

![image](uploads/c38d4156719eb371587de3440a5b1266/image.png)


```DOS
process.pl 001
```

![image](uploads/5fefb2c0c5af77d1e5b6a81153d22eb0/image.png)

O programa `process.pl` pode tomar opções como parâmetros, a fim de realizar apenas parte do processamento ou realizar o processamento em lote para processar várias estações consecutivamente. 

Também é possível usar a opção "#w" quando for necessário modificar a configuração dos sensores usados no CTD.

```DOS
c:\Cruises\PIRATA\PIRATA-FR32\data-processing\CTD>process.pl -h

usage:   perl process.pl [options] first_station [last_station|"#w"]
Options:
    --help                 Display this help message
    --debug                display command, don't process
    --btl                  process sample
    --codac                process realtime data for CODAC
    --pmel                 process realtime data for PMEL
    --ladcp                process LADCP data
    --std|ctd              process Seabird cast
    --plot                 process plots
    --report               process Seabird report
    --all                  process all steps, default

example: perl process.pl 1

example: perl m:\ABRACOS2\data-processing\CTD\process.pl 01

example: perl m:\ABRACOS2\data-processing\CTD\process.pl 01 "#w"

example: perl m:\ABRACOS2\data-processing\CTD\process.pl 01 09 --std
```

**<- [WSL](/pt/WSL)** - **[Resumo](/pt/home)** - **[Descrição](pt/Descrição) ->**

