**<- [Descrição](/pt/Descrição)** - **[Resumo](/pt/home)** - **[Documentaçãoes](pt/Documentação) ->**

# Processamento de dados CTD/LADCP

Os dados do CTD e LADCP são registrados localmente no PC de aquisição e depois copiados para a unidade de rede pelo programa Matlab `ctdSeaProcessing` após cada perfil. O script Matlab `ctdSeaProcessing` também é usado para processar os perfis CTD, via `SBEDataProcessing` e LADCP, via software Visbeck. O software utiliza o arquivo de configuração `ctdSeaProcessing_CRUISEid.ini` sob o diretório `data-processing`, que deve ser modificado no início da cruzeiro. O software é então executado no final de cada estação via Matlab.

# Formatação de dados

Atualmente, existe um roteiro genérico Perl para cada instrumento. Os roteiros estão nas pastas de cada instrumento em `data-processing`. Eles permitem a formatação dos dados em diferentes formatos: ASCII, XML, ODV. A configuração dos scripts é descrita em um arquivo externo `config.ini` localizado no diretório `data-processing`. Este arquivo `config.ini` deve ser modificado no início da cruzeiro a fim de preencher corretamente os atributos globais e de acordo com a configuração dos instrumentos de medição. Cada uma das seções ([ctd], [ctd-all], [btl],…) descreve a estrutura dos dados a serem extraídos para os arquivos. Esta formatação é feita usando o script `process-all.sh`.

# Formatação de dados em formato NetCDF

Os dados são então formatados no formato NetCDF através do perl script oceano2oceansites (em "CRUISE/local/sbin/"). As variáveis criadas seguem o código roscópico no final do script "oceano2oceansites.pl", variáveis podem ser adicionadas aqui, se necessário. Esta formatação é feita com o roteiro "process-all.sh".

**Importante**: Todo o processamento é feito a partir de dados copiados na rede, não localmente. Desta forma, você pode ter certeza de que terá todos os dados necessários para realizar o processamento novamente após duplicar e salvar os dados. 

# Início do processamento do Matlab

Para processar os dados CTD/LADCP, vá para o diretório de processamento de dados Adicione o diretório `data-processing\ctdSeaProcessing` ao caminho do Matlab e execute o script `ctdSeaProcessing.m`.

<div align="center">

![image](uploads/325f32fd35eb99b19c97ca4467f04afa/image.png)

</div>

**Notas**:

- Todos os parâmetros necessários são definidos em um arquivo de configuração `.ini`, que deve ser lido na primeira vez em que for utilizado. A configuração é então salva no arquivo `ctdSeaProcessing.mat`. Se você alterar o arquivo `.ini`, você terá que lê-lo novamente para levar em conta as alterações feitas.
- Quando a configuração do CTD mudar de uma cruzeiro para outra, será necessário executar o programa em modo passo a passo.
- Será então necessário salvar a nova configuração para cada programa, para mais informações, veja o [Manual do Usuário Seabird](https://www.seabird.com/asset-get.download.jsa?code=251446).

<div align="center">

![image](uploads/cb7e7e66a678d103a30007c5c5692c47/image.png)

![image](uploads/0950cc5e308e30fa2f6aa18eafd2e57f/image.png)

![image](uploads/bff3f0aac44f1a785de2d03e96fcf3b4/image.png)

![image](uploads/5afd2f0baffc78c6385382c6918ce4e1/image.png)

</div>

# Extração e formatação de dados

Atualmente, existe um roteiro genérico de Perl para cada instrumento. Os roteiros estão nas pastas de cada instrumento em "processamento de dados". Eles permitem a formatação dos dados em diferentes formatos: ASCII, XML, ODV. A configuração dos scripts é descrita em um arquivo externo `config.ini` localizado na raiz do diretório `data-processing`. Este arquivo `config.ini` deve ser modificado no início do cruzeiro a fim de preencher corretamente os atributos globais e certos parâmetros que são uma função da configuração dos instrumentos de medição (por exemplo, a coluna de parâmetros físicos para o CTD. Cada seção ([ctd], [ctd-all], [btl],...) descreve a estrutura dos dados a serem extraídos nos arquivos. Esta formatação é feita com o roteiro [`process-all.sh`](https://forge.ird.fr/us191/crdap/-/blob/master/local/sbin/process-all.sh).

# Transformação de dados para o formato NetCDF

## O que é NetCDF?

NetCDF (Network Common Data Form) é um formato de arquivo para armazenar dados científicos multidimensionais (multivariados), tais como temperatura, salinidade, pressão, oxigênio, etc., no tempo (ou em outra dimensão).

Este formato tem sido adotado como padrão pela comunidade científica internacional para o compartilhamento de dados científicos. Por exemplo, utilizamos a convenção OceanSITES para descrever dados em oceanografia física.

Os dados no formato netCDF são :

- Auto-descrição. Um arquivo netCDF inclui informações sobre os dados que ele contém.
- Portátil. Um arquivo netCDF pode ser acessado por computadores com diferentes formas de armazenar inteiros, caracteres e números de ponto flutuante, independentemente do sistema operacional utilizado (Windows, Unix, OsX)
- Escalável. As interfaces netCDF permitem o acesso eficiente a pequenos subconjuntos de grandes conjuntos de dados em vários formatos, mesmo a partir de servidores remotos.
- Adicionável. Os dados podem ser adicionados a um arquivo netCDF devidamente estruturado sem copiar o conjunto de dados ou redefinir sua estrutura.
- Partilhável. Um escritor e vários leitores podem acessar o mesmo arquivo netCDF simultaneamente.
- Arquivável. O acesso a todas as formas anteriores de dados do netCDF será suportado pelas versões atuais e futuras do software.

## Transformação dos arquivos ASCII para NetCDF

Os dados são então convertidos para o formato NetCDF através do script genérico perl `oceano2oceansites.pl` encontrado em `$CRUISE/local/sbin/`. As variáveis criadas seguem a convenção do código roscópico. As variáveis podem ser adicionadas ou atualizadas aqui, se necessário, no final do script `oceano2oceansites.pl'. 

# Visualização gráfica dos dados

As parcelas são então geradas utilizando scripts Python encontrados em `$CRUISE/local/sbin/python`. Eles geram gráficos dos perfis CTD, XBT e LADCP, executados pelo script shell `python-plots.sh`, depois `plots.py`, e os gráficos de superfície da rota termossalinógrafo TSG, executados por `scatter.py`. Estes roteiros podem ser ajustados durante o cruzeiro. Estas parcelas são executadas utilizando o roteiro `process-all.sh`.

<div align="center">

![image](uploads/56907cb7927f5234696791c49a99c8dc/image.png)

![image](uploads/c7157f325b7f1cdde1c35791bf155ec3/image.png)

![image](uploads/616d7c09500b417c7e852506b751eacf/image.png)

![image](uploads/b931cf0a01eb34a6036d35868999307d/image.png)

</div>

O código está disponível em [Github](https://github.com/jgrelet/Python-plots)

# Geração de arquivos KML do Google Earth

Um arquivo KML pode ser gerado, incluindo a trajetória do navio, a posição dos perfis XBT e CTD com gráficos interativos dos perfis, clicando sobre os "pinos". Este arquivo é feito com o script `oceano2klm.py`, sob `$CRUISE/local/sbin/python`. Este programa utiliza o arquivo de configuração `local.toml` em processamento de dados e utiliza as figuras Python geradas anteriormente.

O código está disponível em [Github](https://github.com/jgrelet/oceano2kml)

## Visualização do arquivo KML localmente

Para visualizar o arquivo KML a bordo, é necessário instalar um servidor web Nginx no PC Linux e ter uma conexão de Internet para carregar os mapas com o Google Earth.


### Copie os arquivos localmente no servidor web Nginx:

É necessário criar links simbólicos sob a raiz no servidor NGINX. É necessário criar uma RESILIÊNCIA de diretório no servidor web e depois criar os 3 seguintes links com o comando `ln -s <source< <dest>` 

Ou:

```bash
    $ cd /var/www/html/RESILIENCE
    $ ln -s /mnt/campagnes/RESILIENCE/data-processing/CELERITE/plots/ CELERITE
    $ ln -s /mnt/campagnes/RESILIENCE/data-processing/CTD/plots/python CTD
    $ ln -s /mnt/campagnes/RESILIENCE/data-processing/THERMO/plots/ THERMO
```

Iniciar, parar, reiniciar o servidor web:

    $ sudo service nginx <start|stop|restart>

### Arquivo de configuração .toml

O script Python `oceano2kml.py` utiliza um arquivo de configuração padrão `config.toml` em processamento de dados/CTD/Tracks. Este arquivo deve ser modificado no início da campanha, de acordo com os parâmetros e os arquivos utilizados. O servidor web não permite que você "navegue" pelo conteúdo de um diretório, portanto é necessário entrar com a URL completa para visualizar uma imagem de seu navegador.

### Visualizando o arquivo KML na Web

De volta ao laboratório, `oceano2kml.py` precisa ser reprocessado com um arquivo de configuração diferente, o arquivo kml todos os arquivos são copiados para o diretório somente leitura do servidor web 'https://www.brest.ird.fr' e o arquivo kml pode ser carregado on-line com o Google Earth a partir da seguinte URL:

[https://www.brest.ird.fr/us191/cruises/pirata-fr32/pirata-fr32.kml](https://www.brest.ird.fr/us191/cruises/pirata-fr32/pirata-fr32.kml)

![image](uploads/b890aad0e2b3965c3c8b20e533111e8c/image.png)

**<- [Descrição](/pt/Descrição)** - **[Resumo](/pt/home)** - **[Documentaçãoes](pt/Documentação) ->**
