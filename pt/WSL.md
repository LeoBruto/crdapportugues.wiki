**<- [Instalação](/pt/Instalação)** - **[Resumo](/pt/home)** - **[DOS](pt/DOS) ->**

# Princípio

O Subsistema Windows para Linux (WSL) permite que usuários Windows rodem um ambiente GNU/Linux, incluindo a maioria das ferramentas de linha de comando, utilitários e aplicativos, diretamente sob Windows, sem modificações e sem o uso de uma máquina virtual tradicional ou configuração de dual-boot.

# Instalando a WSL

Você deve estar executando Windows 10 versão 2004 e superior (Build 19041 e superior) ou Windows 11.

Antes de instalar distribuições Linux para WSL, você deve garantir que o recurso opcional "Windows Subsystem for Linux" esteja habilitado em seu PC:

Open PowerShell como administradora e executável: C:

Abra uma janela PowerShell com uma conta de administrador e execute o comando: 

    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
	
Em seguida, reinicie o computador.

Instale o novo Terminal Windows do Microsoft Store que substituirá o antigo prompt de comando. A versão atual é chamada de "Terminal Windows".

![image](uploads/77c7d0d611e6d4d05ada400623a2c552/image.png)

Veja a documentação no site da Microsoft.

[https://docs.microsoft.com/en-us/windows/wsl/install](https://docs.microsoft.com/en-us/windows/wsl/install)

Você também pode instalar uma distribuição [Ubuntu 20.04](https://www.microsoft.com/en-us/p/ubuntu-2004/9n6svws3rx71#activetab=pivot:overviewtab) ddiretamente da Microsoft store.

![image](uploads/8870c6217187ff97f74cc79399c1d3b0/image.png)

See [https://ubuntu.com/wsl](https://ubuntu.com/wsl)

O primeiro passo a ser dado quando você abre a distribuição Linux recém-instalada é criar uma conta com um nome de usuário e senha. Este nome de usuário e senha são específicos para sua distribuição Linux e não afetam seu nome de usuário Windows. Uma vez que você tenha criado este nome de usuário e senha, a conta torna-se seu usuário padrão para a distribuição e entra automaticamente no momento da inicialização.

Esta conta é considerada o administrador Linux, com a capacidade de executar comandos de administração `sudo` (Super User Do).

Uma vez lançada a sessão Linux `Ubuntu` e criada a conta de usuário, você pode lançar o terminal Windows. Isto abre uma sessão de DOS por padrão. 

![image](uploads/1338e4df49200a1c22cfc1a39433804d/image.png)

No menu (à direita da aba, seta para baixo), selecione a distribuição instalada, aqui: Ubuntu-20.04

![image](uploads/b001566c51fc28066af63503aaba977b/image.png)

# Configuração

Atualizar a distribuição: 

    sudo apt update
    sudo apt upgrade
	
Instale um ambiente X leve como o Xfce (opcional) se você quiser fazer um display gráfico ou gerar imagens gráficas em Python. Instale somente o ambiente Xfce com o comando:

    sudo apt install xfce4 
    sudo apt install vim-gtk
	
[Como instalar o Xfce Desktop 4.16 no Ubuntu 20.04](https://www.edivaldobrito.com.br/como-instalar-o-xfce-desktop-4-16-no-ubuntu-20-04-linux-mint-20/)

**WSL1** Edite o arquivo `~/.bashrc` e adicione a variável de ambiente DISPLAY para redirecionar a exibição gráfica para o servidor Windows X:

    export DISPLAY=:0.0

**WSL2** a seu próprio endereço IP e não compartilha o endereço de loopback local (127.0.0.1); quando você usa a WSL2, você está realmente se conectando a uma máquina virtual WSL2 em vez da máquina Windows subjacente. Portanto, você deve usar o endereço IP atribuído diretamente ao Windows.
Recomendamos que você utilize o endereço IP interno extraído dinamicamente e o atribua à variável de ambiente DISPLAY, com o código a ser adicionado ao seu arquivo de inicialização `.bashrc`:

    export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0: 

Depois de retornar ao console, teremos que execute o seguinte comando para tornar as mudanças efetivas: `source ~/.bashrc` 
    
	. ~/.bashrc

Instale o servidor gratuito VcXsrv X no Windows para exibir a saída gráfica sob X. Veja detalhes no seguinte endereço: [https://doc.ubuntu-fr.org/wsl](https://doc.ubuntu-fr.org/wsl)

Você deve executar o programa XLaunch para configurar o servidor X antes do primeiro uso, você deve desativar o controle de acesso, marque a caixa de seleção: "Disable server access control"

![image](uploads/113a30270a8c5427a539d31b6ad3b542/image.png)

Caso contrário, você poderá receber os seguintes erros: 

    $ xeyes
    Authorization required, but no authorization protocol was specified
    Error: Can't open display: localhost:0.0

Inicie o servidor X sob Windows, depois em uma janela de bash sob Linux, execute o programa xeyes para testar a tela, os olhos devem seguir seu mouse:

![image](uploads/7c7bb3df9a87c8cef32f75ca36af3c90/image.png)

Em seguida, instale os seguintes pacotes que serão necessários para o processamento posterior dos dados científicos utilizados no repositório CRDAP: 

    sudo apt install -y gcc g++ make dos2unix netcdf-bin libnetcdf-dev perl-doc libswitch-perl libdate-manip-perl libxml-libxml-perl libconfig-tiny-perl pdl libpdl-netcdf-perl python3-pip

Instale o módulo Perl Oceano para usar os scripts no repositório CRDAP:

```bash
mkdir -p ~/local/src
cd ~/local/src
git clone https://forge.ird.fr/us191/oceano.git
cd oceano/lib/perl/Oceano
perl Makefile.PL
make
sudo make install
```

# Instalação e configuração do Python com Miniconda

Instalando e configurando Python com Miniconda

Miniconda é um instalador mínimo para a Conda que permite gerenciar as bibliotecas para desenvolvimento em Python, especialmente para análise de dados. É uma versão reduzida do Anaconda que inclui apenas conda, uma versão do Python 3.x mais nova que a distribuição Ubuntu 20.04 e os pacotes dos quais eles dependem.

Em comparação com o gerenciador de pacotes pip, a conda é muito eficiente. Ele gerencia a versão do Python e os pacotes compatíveis com ele de uma maneira ideal.

A Conda também permite criar ambientes virtuais com uma versão específica de Python.

## Instalação e configuração da miniconda

Para a instalação da Miniconda, assumimos que você a instalará como usuário e não como administrador. Em outras palavras, você não precisará de direitos especiais para instalar o Miniconda e os outros módulos necessários:

```bash
mkdir ~/miniconda
cd ~/miniconda
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
```

Em um shell bash, execute a instalação Miniconda com o comando e responda as seguintes perguntas:

    bash Miniconda3-latest-Linux-x86_64.sh -U

Reinicie bash para que ela leve em conta o procedimento de inicialização da conda.

    . ~/.bashrc

Teste a versão de python e conda:

```python
python
Python 3.7.6 (default, Jan  8 2020, 19:59:22)
[GCC 7.3.0] :: Anaconda, Inc. on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> quit()

conda -V
conda 4.8.3
```
## Instalação de módulos científicos Python com Miniconda no ambiente (base):

```bash
conda update conda
conda install pandas
conda install -c conda-forge iris xarray toml netCDF4 simplekml
pip install julian lat-lon-parser
```


## Discos de montagem/partilha

Os drives internos Windows são montados automaticamente sob /mnt/, ou seja, /mnt/c e /mnt/d no meu laptop que tem 2 SSDs.
Para montar um compartilhamento de rede, drive externo ou pen drive, basta digitar o seguinte comando para montar o 2TB E: drive sob /mnt/e :

    sudo mount -t drvfs e: /mnt/e

**Atenção: ** Se o compartilhamento de rede exigir um login/senha, você precisará acessá-la ou compartilhá-la primeiro com o Windows Explorer antes de poder montá-la com o comando de montagem. 

Você também pode ter as ações montadas automaticamente adicionando pontos de montagem ao arquivo `/etc/fstab`:

```
> cat /etc/fstab
LABEL=cloudimg-rootfs   /        ext4   defaults        0 1
M: /mnt/m drvfs defaults 0 0
O: /mnt/o drvfs defaults 0 0
```

Montar distribuição Linux no Windows Explorer como drive X :

    net use X: \\wsl$\Ubuntu-18.04 /PERSISTENT:YES

ou acessar diretamente a distribuição Linux com o \wsl$ 

![image](uploads/f663666a30bce2f6ca76571796c64222/image.png)

O diretório principal:

![image](uploads/173a5d218698c4034a7b48ccf1e94b4a/image.png)


Para mais informações sobre wsl, consulte o [post](https://us191.ird.fr/spip.php?article77).

**<- [Instalação](/pt/Instalação)** - **[Resumo](/pt/home)** - **[DOS](pt/DOS) ->**
