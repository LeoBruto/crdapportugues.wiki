**<- [Introduction](/fr/Home)** - **[Sommaire](/fr/home)** - **[WSL](/fr/WSL) ->**

# CRDAP - Installation - Version Française

## Prérequis

L'installation de CRDAP nécessite l'installation des logiciels suivants sur un PC Windows:

- Git et éventuellement une interface shell Windows Tortoise Git.
- Matlab (interface graphique pour l'automatisation, le traitement CTD et LADCP)
- WSL (Windows Subsystem for Linux) ou un hôte Linux (utilisé pour la collecte des données, le traitement en temps réel, les conversions de fichiers NetCDF, le tracé automatique des profils et des sections)
- Obtenir et installer les logiciels [Seasoft](https://www.seabird.com/static/list-items/seasoft-2-3-0-en.jsp) comprenant le programme Seasave pour l'acquisition des données des instruments Seabird (CTD) et l'ensemble de logiciels SBEDataProcessing  pour le post-traitement des données, incluant la conversion des données brutes, le filtrage, la réduction et les tracés des profils de station.
- WinADCP et RDI-Tools BBtalk pour configurer, vérifier et télécharger les données des courantomètres doppler LADCP.

**Notes:**

- Le logiciel d'acquisition de données Seasave ne peut fonctionner que sur un PC Windows car il utilise les E/S du PC (série/USB, GPIB). Cependant, le paquet SBEDataProcessing peut être émulé sur un PC Linux en installant le logiciel avec [Wine](https://wiki.winehq.org/Download) ou [Parallels](https://www.parallels.com/fr/) sous Mac OsX.
- Les données LADCP (Lowered Acosutic Doppler Current Profiler) sont traitées avec la version 10.16 du logiciel IFM-GEOMAR/LDEO (Visbeck, 2001), développé par Martin Visbeck et maintenu par Gerd Krahmann.

Lien vers la documentation: [Acquisition and Processing of LADCP Data](https://www.ldeo.columbia.edu/~ant/LADCP.html)

Ce logiciel nécessite l'utilisation d'une licence Matlab.
Nous utilisons également Matlab pour automatiser le traitement d'une station CTD/LADCP Seabird avec une interface utilisateur graphique (GUI).

Si vous ne disposez pas d'un LADCP dans votre chaîne de traitement, ni d'une licence Matlab, il est possible de réaliser le traitement Seabird sous DOS en installant Perl et certains outils Unix portés sur Windows. Voir la page dédiée pour cette installation.

## [Installation de WSL (Windows Subsystem for Linux) ](fr/WSL) 

## [Installation sous DOS avec l'outil GnuWin32](fr/DOS) 


## Récupération  des fichiers sources

### Cloner le dépot en ligne de commande:

    > cd \<your cruise directory\>

    > git clone --recurse-submodules https:\\forge.ird.fr/us191/crdap.git  .
	
### Avec Tortoise Git

<div align="center">

![image](uploads/e60c0002a35c49340b26908a1e834945/image.png)

</div>

Le dépot dera cloné par défaut dans le repertoire crdap (.) mais vous pouvez spécifier un nom de campagne par exemple:

<div align="center">

![image](uploads/dcb514c76913c15f1cfd638febfc49ae/image.png)

![image](uploads/7e5f401cdd4008a521dbe081e8bd1950/image.png)

![image](uploads/4342cfb2b5040e123ec5d9d5a4686b97/image.png)

</div>

Quand la copie est terminée sans erreur, se placer dans le répertoire de destination.

<div align="center">

![image](uploads/d0d8612472802a3a096f4a995301a19e/image.png)

</div>

Le référentiel CRDAP comprend deux sous-modules :

* ctdSeaprocessing : une interface Matlab pour automatiser le traitement d'une bathysonde CTDO2/LADCP. 
* ladcp : Module de traitement des données de courant LADCP, avec une branche atsea simplifiée une une complète, master, permettant autre autre le forcage LADCP/SADCP.

```git
[submodule "data-processing/ctdSeaProcessing"]
	path = data-processing/ctdSeaProcessing
	url = https://github.com/US191/ctdSeaProcessing.git
    branch = master
[submodule "data-processing/LADCP/process"]
	path = data-processing/LADCP/process
	url = https://git.outils-is.ird.fr/rousselot/ladcp.git
	branch = master
```

Après une mises à jour, il sera nécessaire faire un update de ces submodules avec le menu contextuel, pour cela sélectionner "Submodule update".

<div align="center">

![image](uploads/3d0c0a19bb2e5d19d43e8226cf10bf1b/image.png)

</div>

and click "Ok"

<div align="center">

![image](uploads/0cc3f9ff3aa6260d05a59c86cac6591c/image.png)

</div>

Basculer sur la branche THALASSA

    > git checkout THALASSA

Basculer sur la branche SWINGS

    > git checkout SWINGS
	
Revenir à la branche master

    > git checkout master


**<- [Introduction](/fr/Home)** - **[Sommaire](/fr/home)** - **[WSL](/fr/WSL) ->**