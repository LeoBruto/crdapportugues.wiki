# Introduction

Ce dépôt permet de créer l'ensemble des répertoires et des outis permettant de traiter les données acquises en mer lors d'une campagne océanographique.

On trouvera au sein de ce dépôt différentes branches correspondant aux systèmes d'acquisition présents sur chaquun des navires.

# Préambule

Lors d'une campagne océanographique, nous utilisons nos propres instruments ainsi que ceux disponibles à bord. Les systèmes d'acquisition de données et leurs formats sont généralement hétérogènes. Afin de simplifier le traitement et la validation des données, nous avons mis en place ces dernières années un système d'information qui nous permet de structurer et de hiérarchiser l'accès aux informations. Ce système peut être adapté à tout navire et dupliqué de retour au laboratoire, pour finaliser le traitement et l'ajustement des données.

# [Installation](fr/Installation)

### [WSL](fr/WSL)

### [DOS](fr/DOS)

# [Description](fr/Description)

# [Traitements](fr/Traitements)

# [Documentation](fr/Documentation)

# [Notes de version](https://forge.ird.fr/us191/CRDAP/-/blob/master/ReleaseNotes.md)