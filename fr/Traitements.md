**<- [Description](/fr/Description)** - **[Sommaire](/fr/home)** - **[Documentation](/fr/Documentation) ->**

# Traitements des données CTD/LADCP

Les données de la CTD + LADCP sont enregistrées localement sur le PC d’acquisition puis copiées sur le disque réseau par le programme Matlab « ctdSeaProcessing » après chaque profil. Le script Matlab « ctdSeaProcessing » permet également de réaliser le traitement des profils CTD (via SBEDataProcessing) et LADCP (via logiciel Visbeck). Le logiciel utilise le fichier de configuration ctdSeaProcessing_CRUISEid.ini (sous data-processing) qui doit être modifié em début de campagne. Le logiciel est ensuite lancé à chaque fin de station via matlab.

# Démarrage du traitement Matlab

Pour effectuer le traitement des données CTD/LADCP, allez dans le répertoire data-processing Ajoutez le répertoire `data-processing\ctdSeaProcessing` dans le chemin Matlab et exécutez le script `ctdSeaProcessing.m`.

<div align="center">

![image](uploads/325f32fd35eb99b19c97ca4467f04afa/image.png)

</div>

**Notes**:

- L'ensemble des paramètres nécessaires sont définis dans un fichier de configuration .ini, ce dernier doit être lu lor de la première utilisation. La configuration est ensuite sauvegardée dans le fichier ctdSeaProcessing.mat. Si vous modifier le fichier .ini, vous devrez le relire pour prendre en compte les modifications apportées.
- Lorsque la configuration de la CTD change d'une campagne à l'autre, il sera nécessaire de lancer le programme en mode step by step.
- Il faudra alors sauvegarder la nouvelle configuration pour chaque programme, pour plus d'informations, e repporter au [Manuel utilisateur de Seabird](https://www.seabird.com/asset-get.download.jsa?code=251446).

<div align="center">

![image](uploads/cb7e7e66a678d103a30007c5c5692c47/image.png)

![image](uploads/0950cc5e308e30fa2f6aa18eafd2e57f/image.png)

![image](uploads/bff3f0aac44f1a785de2d03e96fcf3b4/image.png)

![image](uploads/5afd2f0baffc78c6385382c6918ce4e1/image.png)

</div>


# Extraction et formatage des données en ASCII

Actuellement, il y a un script Perl générique pour chaque instrument. Les scripts sont dans les dossiers de chaque instrument sous `data-processing`. Ils permettent la mise en forme des données dans différents formats : ASCII, XML, ODV. La configuration des scripts est décrite dans un fichier externe `config.ini` situé à la racine du répertoire `data-processing`. Ce fichier `config.ini` doit être modifié au début de la croisière afin de renseigner correctement les attributs globaux et certains paramètres qui sont fonction de la configuration des instruments de mesure (colonne des paramètres physiques par eemple pour la CTD. Chaque section ([ctd], [ctd-all], [btl],...) décrit la structure des données à extraire dans les fichiers. Cette mise en forme se fait avec le script [`process-all.sh`](https://forge.ird.fr/us191/crdap/-/blob/master/local/sbin/process-all.sh).

# Formatage des données au format NetCDF

Les données sont ensuite formatées au format NetCDF via le script perl oceano2oceansites (sous « CRUISE/local/sbin/ »). Les variables créées suivent le code roscop situées en fin du script « oceano2oceansites.pl », des variables peuvent être ici ajoutées le cas échéant. Ce formatage est effectué à l’aide du script « process-all.sh ».

**Important**: Tous les traitements du "post processing" sont effectués à partir de données copiées sur le réseau, et non localement sur le(s) PC d'acquisition. De cette façon, vous êtes sûr de disposer de toutes les données nécessaires pour effectuer à nouveau le traitement dans un autre environement ou de retour au laboratoire, après avoir bien sur dupliqué et sauvegardé les données sur des supports externes.



# Tracés de données

Les graphiques sont ensuite générés à l'aide de scripts Python qui se trouvent sous `$CRUISE/local/sbin/python`. Ils génèrent d'une part les tracés des profils CTD, XBT et LADCP, lancés par le script shell `python-plots.sh`, puis `plots.py`, et d'autre part les tracés de surface de la route du thermosalinographe TSG réalisé par `scatter.py`. Ces scripts peuvent être ajustés pendant la croisière. Ces tracés sont lancés à l'aide du script `process-all.sh`.

<div align="center">

![image](uploads/56907cb7927f5234696791c49a99c8dc/image.png)

![image](uploads/c7157f325b7f1cdde1c35791bf155ec3/image.png)

![image](uploads/616d7c09500b417c7e852506b751eacf/image.png)

![image](uploads/b931cf0a01eb34a6036d35868999307d/image.png)

</div>

Le code source est disponible sous [Github](https://github.com/jgrelet/Python-plots)

# Generation des fichiers Google Earth KML

Un fichier KML peut être généré, incluant la trajectoire du navire, la position des profils XBT et CTD avec des tracés interactifs des profils en cliquant sur les "punaises". Ce fichier est réalisé avec le script `oceano2klm.py`, sous `$CRUISE/local/sbin/python`. Ce programme utilise le fichier de configuration `local.toml` sous data-processing et reprends les figures Python générées précédemment.

Le code source est disponible sous  [Github](https://github.com/jgrelet/oceano2kml)

## Visualisation du fichier KML en local

Pour visualiser le fichier KML à bord, il est nécessaire d'installer un serveur web Nginx sur le PC Linux et de disposer d'une connexion Internet pour le chargement des cartes avec Google Earth


### Copier localement les fichiers sur le serveur Nginx :

Il est nécessaire de créer des liens symboliques sous la racine sur le serveur NGINX. Il faut créer un répertoire RESILIENCE sur le serveur web puis créer les 3 liens suivants avec la commande `ln -s <source< <dest>` 

Soit:

```bash
    $ cd /var/www/html/RESILIENCE
    $ ln -s /mnt/campagnes/RESILIENCE/data-processing/CELERITE/plots/ CELERITE
    $ ln -s /mnt/campagnes/RESILIENCE/data-processing/CTD/plots/python CTD
    $ ln -s /mnt/campagnes/RESILIENCE/data-processing/THERMO/plots/ THERMO
```

Start, stop, restart le serveur web:

    $ sudo service nginx <start|stop|restart>

### Fichier de configuration .toml

Le script Python `oceano2kml.py` utilise un fichier de configuration par défaut `config.toml` sous data-processing/CTD/Tracks. Ce fichier doit être modifié au début de la campagne en fonction des paramètres et des fichiers utilisés. Le serveur Web ne permet pas de "naviguer" dans le contenu d'un répertoire, il est donc nécessaire de saisir l'URL complète pour visualiser une image depuis son navigateur.

### Visualisation du fichier KML sur le Web

De retour au laboratoire, il faut retraiter `oceano2kml.py` avec un fichier de configuration différent, le fichier kml tous les fichiers sont copiés dans le répertoire en lecture seule du serveur web 'https://www.brest.ird.fr' et le fichier kml peut être chargé en ligne avec Google Earth depuis l'URL suivante : 

[https://www.brest.ird.fr/us191/cruises/pirata-fr32/pirata-fr32.kml](https://www.brest.ird.fr/us191/cruises/pirata-fr32/pirata-fr32.kml)

![image](uploads/b890aad0e2b3965c3c8b20e533111e8c/image.png)

**<- [Description](/fr/Description)** - **[Sommaire](/fr/home)** - **[Documentation](/fr/Documentation) ->**