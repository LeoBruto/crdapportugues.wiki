**<- [WSL](/fr/WSL)** - **[Sommaire](/fr/home)** - **[Description](/fr/Description) ->**

# DOS processing

Cette version plus ancienne fonctionne sous DOS dans une fenêtre « command.com » et permet de réaliser le traitement par lot. 
Le traitement par lot est réalisé actuellement par le script Perl `process.pl` mais il pourrait être facilement remplacé par un script Python, 
voir un exécutable, simplifiant ici la configuration.

Le traitement d'une station CTD/LADCP est réalisé en trois étapes:

- script `copy.bat` qui réalise la copie des fichiers bruts issus de Seasave (CTD) et bbtalk (LADCP) vers les repertoires `data-raw` et `data-processing` de CRDAP.
- conversion et traitement des fichiers CTD avec le script Perl `process.pl` utilise les programmes `SBEDataProcessing`
- lancement manuel du traitement d'un profil LADCP avec le script Matlab `process_cast(xxx)` 

## Installation des programmes et outils

Installer le langage Perl à partir de la distribution ActiveState :
https://www.activestate.com/products/perl/downloads/

Depuis cette page, il faudra créer un compte et configurer l’installateur (OS, package additionnel, etc)
Ou plus simple, installer l’outils “state”, avec la commande powershell suivante :

Exécutez la commande suivante en tant qu'administrateur dans votre Invite de commande pour l'installer:

    powershell "IEX(New-Object Net.WebClient).downloadString('https://platform.activestate.com/dl/cli/install.ps1')"
	
Configurez ce projet localement avec la commande suivante :

    state activate <your login>/CTD
	
Installer également depuis internet les utilitaires bzip2 et gawk, pour Windows, depuis le site GnuWin32, binaires d’installation à récupérer sous :

http://gnuwin32.sourceforge.net/packages/bzip2.htm
http://gnuwin32.sourceforge.net/packages/gawk.htm

Le script de traitement `process.pl` fait appel à la fin de programme SbeConReport.exe, pour que ce dernier fonctionne, il faut rajouter le chemin de SBEDataProcessing-Win32 dans le path Windows, ainsi que le chemin des utilitaires GnuWin :

    Path=C:\Program Files (x86)\Sea-Bird\SBEDataProcessing-Win32;C:\Program Files (x86)\GnuWin32\bin;
	
Vous pouvez utiliser l'outil [Rapid Environment Editor](https://www.rapidee.com/en/download) (Windows environment variables management):

# Configuration

## Définir les variables d'environnement

Pour un accès rapide au répertoire de traitement et au script, il est conseillé de définir les variables suivantes comme on le ferait sous Linux en ligne de commande ou dans son fichier `autoexec.bat` afin qu'elles soient définies une fois pour toute.

    set CRUISE=PIRATA-FR32
    set CRUISE_FORMAT=fr32%03d
    set DRIVE=c:\Cruises\PIRATA
    set CTD=%DRIVE%\%CRUISE%\data-processing\CTD

et de rajouter le chemin d'accès dans la variable `%path%` aux scripts perl et batch qui se trouvent dans le répertoire `local\sbin\dos` 

    setx path "%path%;%DRIVE%\%CRUISE%\local\sbin\dos"

Le script `process.pl` utilise exactement la même configuration et les mêmes répertoires que l'interface graphique Matlab `ctdSeaProcessing`. 

# Utilisation

Ouvrir une fenêtre `cmd`, définir les variables d'environnement pour la campagne, se déplacer dans le répertoire `data-processing\CTD` puis lancer le traitement de la station 1:

```DOS
set CRUISE=PIRATA-FR32
set PREFIX=fr32
set PREFIXM=FR32
set CRUISE_FORMAT=%PREFIX%%03d
set DRIVE=c:\Cruises\PIRATA
set CTD=%DRIVE%\%CRUISE%\data-processing\CTD
cd %CTD%
synchro-ctd.bat 001
```

![image](uploads/c38d4156719eb371587de3440a5b1266/image.png)


```DOS
process.pl 001
```

![image](uploads/5fefb2c0c5af77d1e5b6a81153d22eb0/image.png)

Le programme `process.pl` peut prendre des options en paramètre, afin de ne réaliser qu'une partie des traitements, ou bien de réaliser un traitement par lot afin de traiter plusieurs stations consécutivement. 

Il est également possible d'utiliser l'option "#w" lorsqu'il est nécessaire de modifier la configuration des capteurs utilisés sur la CTD.

```DOS
c:\Cruises\PIRATA\PIRATA-FR32\data-processing\CTD>process.pl -h

usage:   perl process.pl [options] first_station [last_station|"#w"]
Options:
    --help                 Display this help message
    --debug                display command, don't process
    --btl                  process sample
    --codac                process realtime data for CODAC
    --pmel                 process realtime data for PMEL
    --ladcp                process LADCP data
    --std|ctd              process Seabird cast
    --plot                 process plots
    --report               process Seabird report
    --all                  process all steps, default

example: perl process.pl 1

example: perl m:\ABRACOS2\data-processing\CTD\process.pl 01

example: perl m:\ABRACOS2\data-processing\CTD\process.pl 01 "#w"

example: perl m:\ABRACOS2\data-processing\CTD\process.pl 01 09 --std
```

**<- [WSL](/fr/WSL)** - **[Sommaire](/fr/home)** - **[Description](/fr/Description) ->**
