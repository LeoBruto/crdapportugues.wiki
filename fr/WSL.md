**<- [Installation](/fr/Installation)** - **[Sommaire](/fr/home)** - **[DOS](/fr/DOS) ->**

# Principe

Windows Subsystem for Linux (WSL) permet aux utilisateurs Windows d'exécuter un environnement GNU/Linux, incluant la plupart des outils en ligne de commande, les utilitaires et applications, directement sous Windows, sans modification, et sans l'utilisation d'une machine virtuelle traditionnelle ou d'une configuration à double démarrage.

# Installation de WSL

Vous devez exécuter Windows 10 version 2004 et supérieure (Build 19041 et supérieur) ou Windows 11.

Avant d'installer des distributions Linux pour WSL, vous devez vous assurer que la fonction optionnelle "Windows Subsystem for Linux" est activée sur votre PC:

Ouvrez PowerShell en tant qu'administrateur et exécutez : C:

Ouvrir une fenêtre PowerShell avec un compte administrateur et exécuter la commande: 

    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
	
Redémarrer ensuite l'ordinateur.

Installez le nouveau terminal Windows depuis le Microsoft Store qui remplacera avantageusement l'ancienne invite de commande. La version actuelle s'appelle "Windows Terminal".

![image](uploads/77c7d0d611e6d4d05ada400623a2c552/image.png)

Consultez la documentation sur le site Web de Microsoft.

[https://docs.microsoft.com/en-us/windows/wsl/install](https://docs.microsoft.com/en-us/windows/wsl/install)

Vous pouvez également installer une distribution [Ubuntu 20.04](https://www.microsoft.com/en-us/p/ubuntu-2004/9n6svws3rx71#activetab=pivot:overviewtab) directement depuis le Microsoft store.

![image](uploads/8870c6217187ff97f74cc79399c1d3b0/image.png)

See [https://ubuntu.com/wsl](https://ubuntu.com/wsl)

La première étape qui vous est demandée lorsque vous ouvrez la distribution Linux nouvellement installée est de créer un compte avec un nom d'utilisateur et un mot de passe. Ce nom d'utilisateur et ce mot de passe sont spécifiques à votre distribution Linux et n'affectent pas votre nom d'utilisateur Windows. Une fois que vous avez créé ce nom d'utilisateur et ce mot de passe, le compte devient votre utilisateur par défaut pour la distribution et se connecte automatiquement au moment du démarrage.

Ce compte est considéré comme l'administrateur Linux, avec la possibilité d'exécuter des commandes d'administration sudo (Super User Do).

Une fois que la session Linux (Ubuntu) est lancée et que le compte utilisateur est créé, vous pouvez lancer le terminal Windows. Celui-ci ouvre par défaut une session DOS. 

![image](uploads/1338e4df49200a1c22cfc1a39433804d/image.png)

Dans le menu (à droite de l'onglet, flèche vers le bas), sélectionnez la distribution installée, ici : Ubuntu-20.04

![image](uploads/b001566c51fc28066af63503aaba977b/image.png)

# Configuration

Mettre à jour la distribution: 

    sudo apt update
    sudo apt upgrade
	
Installez un environnement X léger tel que Xfce (facultatif) si vous voulez faire un affichage graphique. Installez uniquement l'environnement Xfce avec la commande:

    sudo apt install xfce4 
    sudo apt install vim-gtk

**WSL1** Modifiez le fichier `~/.bashrc` et ajoutez la variable d'environnement DISPLAY pour rediriger l'affichage graphique vers le serveur X Windows:

    export DISPLAY=:0.0

**WSL2** à sa propre adresse IP et ne partage pas l'adresse de loopback localhost (127.0.0.1); lorsque vous utilisez WSL2, vous vous connectez en fait à une machine virtuelle WSL2 plutôt qu'à la machine Windows sous-jacente. Vous devez donc utiliser directement l'adresse IP attribuée à Windows.
Nous vous recommandons d'utiliser l'adresse IP interne extraite dynamiquement et de l'affecter à la variable d'environnement DISPLAY, avec le code à ajouter à votre fichier d'initialisation `.bashrc`:

    export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0: 

Ressourcer le fichier `~/.bashrc` pour prendre en compte la nouvelle variable DISPLAY:
    
	. ~/.bashrc

Installez le serveur X gratuit VcXsrv sous Windows pour afficher une sortie graphique sous X. Voir les détails à l'adresse suivante: [https://doc.ubuntu-fr.org/wsl](https://doc.ubuntu-fr.org/wsl)

Vous devez exécuter le programme XLaunch pour configurer le serveur X avant la première utilisation, vous devez désactiver le contrôle d'accès, cocher la case: "Disable server access control"

![image](uploads/113a30270a8c5427a539d31b6ad3b542/image.png)

Sinon, vous risquez d'obtenir les erreurs suivantes: 

    $ xeyes
    Authorization required, but no authorization protocol was specified
    Error: Can't open display: localhost:0.0

Démarrez le serveur X sous Windows puis dans une fenêtre bash sous Linux, lancez le programme xeyes pour tester l'affichage, les yeux doivent suivre votre souris :

![image](uploads/7c7bb3df9a87c8cef32f75ca36af3c90/image.png)

Installez ensuite les paquets suivants qui seront nécessaires pour le traitement ultérieur des données scientifiques utilisées dans le référentiel CRDAP: 

    sudo apt install -y gcc g++ make dos2unix netcdf-bin libnetcdf-dev perl-doc libswitch-perl libdate-manip-perl libxml-libxml-perl libconfig-tiny-perl pdl libpdl-netcdf-perl python3-pip

Installer le module Perl Oceano pour utiliser les scripts du référentiel CRDAP:

```bash
mkdir -p ~/local/src
cd ~/local/src
git clone https://forge.ird.fr/us191/oceano.git
cd oceano/lib/perl/Oceano
perl Makefile.PL
make
sudo make install
```

# Installation et configuration de Python avec Miniconda

Miniconda est un installateur minimal pour Conda qui permet de gérer les bibliothèques pour développer en Python, notamment pour l'analyse de données. C'est une version réduite d'Anaconda qui ne comprend que conda, une version de Python 3.x plus récente que la distribution Ubuntu 20.04 et les paquets dont ils dépendent.

Comparé au gestionnaire de paquets pip, conda est très efficace. Il gère la version de Python et les paquets compatibles avec celle-ci de manière optimale.

Conda permet également de créer des environnements virtuels avec une version spécifique de Python.

## Installation et configuration de miniconda

Pour l'installation de Miniconda, nous supposons que vous l'installerez en tant qu'utilisateur, et non en tant qu'administrateur. En d'autres termes, vous n'aurez pas besoin de droits spéciaux pour installer Miniconda et les autres modules nécessaires :

```bash
mkdir ~/miniconda
cd ~/miniconda
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
```

Dans un shell, lancez l'installation de Miniconda avec la commande et répondez aux questions suivantes:

    bash Miniconda3-latest-Linux-x86_64.sh -U

Redémarrez votre shell afin qu'il prenne en compte la procédure d'initialisation de conda.

    . ~/.bashrc

Tester la version de python et conda:

```python
python
Python 3.7.6 (default, Jan  8 2020, 19:59:22)
[GCC 7.3.0] :: Anaconda, Inc. on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> quit()

conda -V
conda 4.8.3
```
## Installation des modules scientifiques Python avec Miniconda dans l'environnement par défaut (base):

```bash
conda update conda
conda install pandas
conda install -c conda-forge iris xarray toml netCDF4 simplekml
pip install julian lat-lon-parser
```


## Montage/partage des disques

Les disques internes de Windows sont automatiquement montés sous /mnt/, c'est-à-dire /mnt/c et /mnt/d sur mon portable qui possède 2 SSD.
Pour monter un partage réseau, un disque externe ou une clé USB, il suffit d'entrer la commande suivante pour monter le disque E : de 2TB sous /mnt/e :

    sudo mount -t drvfs e: /mnt/e

**Attention:** Si le partage réseau nécessite un login/password, il faudra accéder ou se partage au prélable avec l'explorateur Windows avant de pouvoir le monter avec la commande mount. 

On peut également faire en sorte de monter les partages automatiquement en ajoutant les points de montage dans le fichier `/etc/fstab`:

```
> cat /etc/fstab
LABEL=cloudimg-rootfs   /        ext4   defaults        0 1
M: /mnt/m drvfs defaults 0 0
O: /mnt/o drvfs defaults 0 0
```

Montez la racine de la distribution Linux dans l'Explorateur Windows en tant que lecteur X :

    net use X: \\wsl$\Ubuntu-18.04 /PERSISTENT:YES

ou accéder directement à la distribution Linux avec le partage \wsl$ 

![image](uploads/f663666a30bce2f6ca76571796c64222/image.png)

Le répertoire racine:

![image](uploads/173a5d218698c4034a7b48ccf1e94b4a/image.png)



Pour plus d'informations sur wsl, voir le [billet](https://us191.ird.fr/spip.php?article77)

**<- [Installation](/fr/Installation)** - **[Sommaire](/fr/home)** - **[DOS](/fr/DOS) ->**
