**<- [WSL](/fr/WSL)** - **[Sommaire](/fr/home)** - **[Traitements](/fr/Traitements) ->**

# Schéma général du système d'information

|Mise en place de l’environnement utilisateur|
| :--------------- |
|.bashrc|
|local/etc/skel/.bashrc.CRUISENAME|
|contrab|


|Synchronisation des données|Formatage des données (ASCII,XML,ODV) |Formatage des données (NetCDF)|Visualisations des données|Fichiers Google kml|
|:--------------- |:---------------:|:---------------:|:---------------:| ---------:|
|local/sbin/synchro.sh (données bord)|local/sbin/process-all.sh|local/sbin/process-all.sh|local/sbin/process-all.sh|local/sbin/process-all.sh|local/sbin/process-all.sh|
|<font color='red'>data-processing/ctdSeaProcessing.ini</font> (CTD/LADCP)|data-processing/config.ini|local/sbin/oceano2oceansites.pl|local/sbin/python/python-plots.sh|oceano2python.py|
|data-processing/ctdSeaProcessing |data-processing/[CTD,XBT,...]/[ctd,xbt,…].pl||local/sbin/python/scatter.py|data-processing/local.toml|

# Arborescence générale

```bash
├── data-final		    = Produits finis
├── data-adjusted		= Données ajustées 
├── data-processing		= Données prétraitées
├── data-raw			= Données brutes
├── DOCUMENTS			= Documents de campagne
├── local			    = Utilitaires de traitement et de synchronisation
└── tmp			        = Fichiers temporaires
```

# Configuration d’une session de traitement sous Linux

Le principe de cette opération est d’obtenir dans son environnement shell l’ensemble des alias nécessaires aux traitements et à la mise en forme des données.

Dans le fichier `.bashrc` de l'utilisateur `~/.bashrc`, en fin de script, définir les variables d'environnement pour la campagne et le fichier à sourcer :

```bash
# CRUISE variables
export DRIVE=/mnt/campagnes
export CRUISE=RESILIENCE
export CRUISEid=RES

# Source CRUISE definitions
if( -f ${DRIVE}/local/etc/skel/.bashrc.${CRUISE} ) then
  source ${DRIVE}/local/etc/skel/. bashrc.${CRUISE}
endif
```

Le fichier à sourcer se trouve sous `$CRUISE/local/etc/skel/.bashrc.CRUISENAME`. Ce fichier contient tous les alias nécessaires aux traitements. Ce script peut être modifié en début de campagne pour ajouter ou supprimer des alias.

Deux scripts shell sous `$CRUISE/local/sbin/` permettent de lancer l’ensemble des tâches de traitement :

* Le script `synchro.sh` récupère les données sur le réseau du bord et les copies dans les 	répertoires de 	`data-raw` et de `data-processing`. Ce script doit être modifié en début de campagne de manière à s’ajuster à l’architecture réseau du navire. 

* Le script `process-all.sh` va réaliser l’extraction et l’archivage sous différents formats (ASCII, XML, ODV, NetCDF) une fois la collecte effectuée par le script `synchro.sh`.

# Automatisation des traitements

La synchronisation des données ainsi que les traitements sont effectués automatique à l’aide de la crontab. Pour modifier son contenu, utiliser la commande `crontab –e` et  `crontab –l` pour la visualiser. 	

```bash
$ crontab -l 

# .---------------- minute (0 - 59)
# |   .------------- hour (0 - 23)
# |   |   .---------- day of month (1 - 31)
# |   |   |   .------- month (1 - 12) OR jan,feb,mar,apr ...
# |   |   |   |  .----- day of week (0 - 7) (Sunday=0 or 7)  OR sun,mon,tue,wed,thu,fri,sat
# |   |   |   |  |
# *   *   *   *  *  command to be executed
# all processing cruise 3 times per day (local time: 8, 16, 22h)
SHELL=/bin/sh
CRUISE=RESILIENCE
DRIVE=/mnt/campagnes
CRUISEid=RES
5 9,11,17,22 * * *  /mnt/campagnes/${CRUISE}/local/sbin/synchro.sh $CRUISE $DRIVE $CRUISEid > /mnt/campagnes/${CRUISE}/local/logs/synchro.log 2>&1
30 9,11,17,22 * * * /mnt/campagnes/${CRUISE}/local/sbin/process-all.sh $CRUISE $DRIVE > /mnt/campagnes/${CRUISE}/local/logs/process.log 2>&1
0 8,12,21 * * * rsync -av --exclude '.git' /mnt/campagnes/${CRUISE}/ /media/science/WD\ 2T/campagnes/${CRUISE}/ > /mnt/campagnes/${CRUISE}/local/logs/backup.log 2>&1
```

Lors de la mission, vérifier régulièrement les fichiers de log pour voir d’éventuels problèmes (droits en lecture, variables nouvelles,…)

**<- [WSL](/fr/WSL)** - **[Sommaire](/fr/home)** - **[Traitements](/fr/Traitements) ->**
