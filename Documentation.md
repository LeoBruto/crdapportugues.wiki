# Online documentation

A list of documentation in French, is available on the collaborative site of the UAR191 IMAGO, describing in detail the installation of the programs necessary for the proper use of the processing toolchain.

- [CRDAP Simplified CRDAP protocol for deployment and processing](https://drive.ird.fr/s/9TLga4ECiPR2pN5)
- [Instruction décrivant le Système d’Information, les méthodes d’acquisition et de traitement des données en mer et au laboratoire - Campagne PIRATA-FR31 N/O Thalassa](https://drive.ird.fr/s/Sikf5RPnQHPSp7J)
- [Installation manual for the CRDAP and CTD-Sea-Processing processing toolchain](https://drive.ird.fr/s/XCXzLanLZCcr9jB)
- [List of operations for a CTD-O2/LADCP cast](https://drive.ird.fr/s/w3tezepc9kqa2mr)
- [List of operations for a CTD-O2/LADCP cast - oldest in english](https://drive.ird.fr/s/mY3eJWfzHTYZxaM)
- [Protocole de préparation d’une CTD et LADCP](https://drive.ird.fr/s/AY4Qm6ogB8RZC5W)
- [Protocol describing the Information System, the methods of acquisition and processing of data at sea and in the Brest laboratory, 4 years old](https://drive.ird.fr/s/oXMMTryPrBy3tto)
- [Protocole d’installation et préparation d’un PC d’acquisition avant une campagne](https://drive.ird.fr/s/ED7tzLfNLH6qqnF)
- [Mise en œuvre de Veeam EndpointBackup FREE](https://drive.ird.fr/s/pgGLkXL4KMxMCAE)

# External links:

- [Seasoft softwares](https://www.seabird.com/static/list-items/seasoft-2-3-0-en.jsp) de Seabird
- [SBE data processing User manual](https://www.seabird.com/asset-get.download.jsa?code=251446) from Seabird