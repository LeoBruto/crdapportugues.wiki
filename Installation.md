# CRDAP - Installation - English version

## Prerequisite

The installation of CRDAP requires the following software to be installed on the Windows PC:

- Git and optionally a Windows shell interface Tortoise Git
- Matlab (GUI for automation, CTD and LADCP processing)
- WSL (Windows Subsystem for Linux) or a Linux host used for data collection, realtime processing, NetCDF files conversion, automatic plotting of profiles and sections.
- Download and install [Seasoft package](https://www.seabird.com/static/list-items/seasoft-2-3-0-en.jsp) including Seasave program for Seabird instruments data acquisition and SBEDataProcessing software package for data post processing, including raw data conversion, filtering, averaging and plot profiles.
- WinADCP and RDI-Tools BBtalk to setup, check and download data from LADCP instrument

**Notes:**

- The Seasave data acquisition software can only run on a Windows PC because it uses the PC I/O (Serial/USB, GPIB). However, the SBEDataProcessing package can be emulated on a Linux PC by installing the software with [Wine](https://wiki.winehq.org/Download) or [Parallels](https://www.parallels.com) under Mac OsX.
- LADCP (Lowered Acosutic Doppler Current Profiler) data have been processed using version 10.16 of the IFM-GEOMAR/LDEO software (Visbeck, 2001), developed by Martin Visbeck and maintained by Gerd Krahmann.

Link to documentation: [Acquisition and Processing of LADCP Data](https://www.ldeo.columbia.edu/~ant/LADCP.html)

This software requires the use of a Matlab license.
We also use Matlab to automate the processing of a CTD/LADCP Seabird station with a graphical user interface (GUI)

If you do not have a LADCP in your processing chain, nor a Matlab license, it is possible to perform Seabird processing under DOS by installing Perl and some Unix tools ported to Windows. See the dedicated page for this installation.

## [WSL (Windows Subsystem for Linux) installation](WSL) 

## [DOS installation with GnuWin tools](DOS)

## Cloning CRDAP repository 

### With a command line in cmd console ou git bash

    > cd <your cruise directory>
    > git clone --recurse-submodules https://forge.ird.fr/us191/crdap.git .

You can change the target directory name by replace, default (dot) is crdap

    > git clone --recurse-sub-modules https://forge.ird.fr/us191/crdap.git <cruise name>

### With Tortoise Git

<div align="center">

![image](uploads/e60c0002a35c49340b26908a1e834945/image.png)

</div>

You can change the target name directory, default is crdap

![image](uploads/dcb514c76913c15f1cfd638febfc49ae/image.png)

To download submodules, select button check "Recursive", as the same effect an option *--recurse-submodules*

<div align="center">

![image](uploads/7e5f401cdd4008a521dbe081e8bd1950/image.png)

![image](uploads/4342cfb2b5040e123ec5d9d5a4686b97/image.png)

</div>

When everything is fine, go to the working directory, here crdap

![image](uploads/d0d8612472802a3a096f4a995301a19e/image.png)

The CRDAP repository now includes two additional submodules:

* **ctdSeaprocessing:** a Matlab interface to automate the processing of a CTDO2/LADCP bathysonde. 
* **ladcp:** LADCP current data processing module, with a simplified processing atsea branch or full master branch including SDACP/LADCP integration.

```git
[submodule "data-processing/ctdSeaProcessing"]
	path = data-processing/ctdSeaProcessing
	url = https://github.com/US191/ctdSeaProcessing.git
    branch = master 
[submodule "data-processing/LADCP/process"]
	path = data-processing/LADCP/process
	url = https://git.outils-is.ird.fr/rousselot/ladcp.git
	branch = master 
```

The two submodules have been downloaded. After an update, it will be necessary to update the submodules with a contextual menu, select "Submodule Update"

![image](uploads/3d0c0a19bb2e5d19d43e8226cf10bf1b/image.png)

and click "Ok"

![image](uploads/0cc3f9ff3aa6260d05a59c86cac6591c/image.png)

Switch to THALASSA branch

    > git checkout THALASSA

Switch to SWINGS branch

    > git checkout SWINGS

go back to master branch

    > git checkout master




